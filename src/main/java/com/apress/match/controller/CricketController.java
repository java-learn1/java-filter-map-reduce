package com.apress.match.controller;


import com.apress.match.data.MatchRepository;
import com.apress.match.model.Match;
import com.apress.match.model.MatchSummary;
import com.apress.match.model.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value= "/api/matches")
public class CricketController {

    Logger log = LoggerFactory.getLogger(CricketController.class);

    private MatchRepository repo = new MatchRepository();
    @GetMapping
    public List<Match> getMatches(){ return repo.getMatches();}

    @GetMapping(path = "/{team}")
    public ResponseEntity<List<Match>> getTeamMatches(@PathVariable(name="team") String team,
                                                      @RequestParam(name="oponnent", required = false) String oponnent){
        List<Match> matches = repo.getMatches().stream()
                .filter(m->{
                    return m.getInfo().getTeams().contains(team);
                })
                .filter(m->{
                    return oponnent == null || m.getInfo().getTeams().contains(oponnent);
                })
                .collect(Collectors.toList());

        return ResponseEntity.ok()
                .header( "count", String.format("%d", matches.size()))
                .body(matches);
    }

    @GetMapping(path = "/{team}/summaries")
    public ResponseEntity<List<MatchSummary>> getSummaries(@PathVariable(name="team") String team,
                                                      @RequestParam(name="oponnent", required = false) String oponnent){
        List<MatchSummary> matches = repo.getMatches().stream()
                .filter(m-> m.getInfo().getTeams().contains(team))
                .filter(m-> oponnent == null || m.getInfo().getTeams().contains(oponnent))
                .map(m->{
                    MatchSummary sumary = new MatchSummary(m.getInfo().getDates().get(0),
                            m.getInfo().getCity(),
                            m.getInfo().getTeams(),
                            m.getInfo().getOutcome().getWinner());
                    return sumary;
                })
                .collect(Collectors.toList());

        return ResponseEntity.ok()
                .header( "count", String.format("%d", matches.size()))
                .body(matches);
    }

    @GetMapping(path = "/{team}/record")
    public Record getWinLossRecord(@PathVariable(name="team") String team,
                                   @RequestParam(name="oponnent", required = false) String oponnent){

        long startTime = System.currentTimeMillis();
        Record winLossRecord = repo.getMatches().parallelStream()
                .filter(m-> m.getInfo().getTeams().contains(team))
                .filter(m-> oponnent == null || m.getInfo().getTeams().contains(oponnent))
                .filter(m-> !"no result".equalsIgnoreCase(m.getInfo().getOutcome().getResult()))
                .reduce(new Record(team),
                        (r,m)->{
                            if (team.equalsIgnoreCase(m.getInfo().getOutcome().getWinner())) {
                                r.setWins(r.getWins() + 1);
                            } else if ("tie".equalsIgnoreCase(m.getInfo().getOutcome().getResult())) {
                                r.setTies(r.getTies() + 1);
                            } else {
                                r.setLosses(r.getLosses() + 1);
                            }
                            return r;
                        }, (a, b) -> {  // This is a combiner used if Parrallel streams are used
                            return new Record(a.getTeam(),
                                    a.getWins() + b.getWins(),
                                    a.getLosses() + b.getLosses(),
                                    a.getTies() + b.getTies());
                        });

        long endTime = System.currentTimeMillis() - startTime;
        log.info("Time: " + endTime + " ms");
        return winLossRecord;
    }

    @PostMapping
    public void addMatch(@RequestBody Match match){
        if(match != null){
            repo.getMatches().add(match);
        }
    }
}
