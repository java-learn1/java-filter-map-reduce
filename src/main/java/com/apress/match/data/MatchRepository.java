package com.apress.match.data;

import com.apress.match.model.Match;

import java.util.ArrayList;
import java.util.List;

public class MatchRepository {

    private List<Match> matches;

    public MatchRepository(){
        matches = new ArrayList<>();
    }

    public List<Match> getMatches() {
        if(matches == null){
            matches = new ArrayList<>();
        }
        return matches;
    }

    public void add(Match match){
        if(matches == null){
            matches = new ArrayList<>();
        }
        matches.add(match);
    }
}
